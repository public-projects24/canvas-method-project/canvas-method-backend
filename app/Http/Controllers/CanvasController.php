<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CanvasRequest;
use App\Http\Resources\CanvasResource;

class CanvasController extends Controller
{
    public function index()
    {
        return response()->json(
            new CanvasResource(request()->user()->canvas), 200
        );
    }

    public function update(CanvasRequest $request)
    {
        try {
            $user = $request->user();
            $user->canvas()->update([
                $request->field => $request->value
            ]);

            return response()->json([
                'message' => 'Canvas ' . $request->field . ' updated.'
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'message' => 'an error occurred while trying to update canvas.',
                'description' => config('app.debug') ? $e->getMessage() : 'Please activate debug mode to see the error message.'
            ], 500);
        }
    }
}
