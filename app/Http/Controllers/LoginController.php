<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'invalid login credentials.'
            ], 401);
        }

        $user = User::where('email', $request->email)->firstOrFail();
        return [
            'token' => $user->createToken('auth-token')->plainTextToken,
        ];
    }
}
