<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CanvasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'competencies' => $this->competencies,
            'product' => $this->product,
            'resources' => $this->resources,
            'standards' => $this->standards,
            'tasks' => $this->tasks,
            'tools' => $this->tools,
            'methods' => $this->methods,
            'difussion' => $this->difussion,
            'grouping' => $this->grouping,
        ];
    }
}
