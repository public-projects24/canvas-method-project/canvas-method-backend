<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;


    public function test_user_can_login()
    {
        $data = [
            'email' => 'user1@gmail.com',
            'password' => '123456',
        ];

        $response = $this->postJson('/api/login', $data);
        //$response->dd();

        $response->assertStatus(200)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll(['token']);
            }
        );
    }

    public function test_user_can_not_login_with_empty_fields()
    {
        $data = [
            'email' => '',
            'password' => '',
        ];

        $response = $this->postJson('/api/login', $data);
        //$response->dd();
        $response->assertUnprocessable();
        $response->assertInvalid(['email', 'password']);

    }
}
