<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CanvasTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    public function test_user_can_get_canvas()
    {
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->getJson('/api/canvas', $headers);
        //$response->dd();
        $response->assertStatus(200)
        ->assertJson(
            function (AssertableJson $json) {
                $json->hasAll([
                    'competencies',
                    'product',
                    'resources',
                    'standards',
                    'tasks',
                    'tools',
                    'methods',
                    'difussion',
                    'grouping',
                ]);
            }
        );
    }

    public function test_user_can_update_canvas_field()
    {
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        $data = [
            'field' => 'competencies',
            'value' => 'comp'
        ];

        $response = $this->putJson('/api/canvas', $data, $headers);
        $response->assertStatus(200);
        $this->assertEquals('comp', $user->fresh()->canvas->competencies);
    }

    public function test_user_can_not_update_canvas_with_field_empty()
    {
        $user = User::where('id', 1)->first();
        $token = $user->createToken('auth-token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        $data = [
            'field' => '',
            'value' => 'comp'
        ];

        $response = $this->putJson('/api/canvas', $data, $headers);
        $response->assertUnprocessable();
        $response->assertInvalid(['field']);
    }
}
