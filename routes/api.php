<?php

use App\Http\Controllers\CanvasController;
use App\Http\Controllers\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

Route::post('/login', [LoginController::class, 'index']);

Route::middleware('auth:sanctum')->group(function() {
    Route::get('/canvas', [CanvasController::class, 'index']);
    Route::put('/canvas', [CanvasController::class, 'update']);
});
