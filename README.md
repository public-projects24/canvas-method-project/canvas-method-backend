## Tablero Canvas (Backend)

Este proyecto contiene los endopoints necesearios para autenticar a un usuario, obtener y editar los datos del tablero canvas.

## Especificaciones

Este proyecto esta hecho en laravel 9. Necesitas tener instalado php version >= 8.0, composer y mysql para ejecutarlo.

## Instalación

1. Clona este repositorio en tu local
2. Ingresa a la carpeta del proyecto e instala los paquetes con composer, ejectuando el siguiente comando en la terminal.

```
composer install
```

3. Duplica el archivo .env.example que está en la raiz y renombralo por .env
4. Crear una base de datos con el nombre que desees, charset "utf8mb4" y collation "utf8mb4_unicode_ci"
5. Edita el archivo .env y modifica los datos de conexión a la base de datos según el punto anterior.
6. Genera una llave para la aplicacion con artisan, ejecutando el siguiente comando en la terminal.

```
 php artisan key:generate
```

7. Ejecuta los tests

```
 php artisan test
```

8. Ejectua las migraciones y seeder, corriendo el siguiente comando en la terminal.

```
 php artisan migrate --seed
```

9. Listo, puedes probarla.
