<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canvases', function (Blueprint $table) {
            $table->id();
            $table->text('competencies')->nullable();
            $table->text('product')->nullable();
            $table->text('resources')->nullable();
            $table->text('standards')->nullable();
            $table->text('tasks')->nullable();
            $table->text('tools')->nullable();
            $table->text('methods')->nullable();
            $table->text('difussion')->nullable();
            $table->text('grouping')->nullable();
            $table->foreignId('user_id')->constrained()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canvases');
    }
};
